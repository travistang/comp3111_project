﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegistrationPage.aspx.cs" Inherits="HKeInvestWebApplication.RegistrationPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Create a new login account</h2>

    <h4>Client Information</h4>

    <div class="form-horizontal">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" EnableClientScript="False" />

        <h4>Account Type</h4>
        <div class="form-group">

            <asp:DropDownList ID="ddlAccountType" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">Account Type</asp:ListItem>
                <asp:ListItem Value="individual">Individual.</asp:ListItem>
                <asp:ListItem Value="survivor">Survivorship</asp:ListItem>
                <asp:ListItem Value="common">Common</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
         <h4>Client Information</h4>
        </div>

        <div class="form-group">

        <asp:Label runat="server" Text="First Name" AssociatedControlID="FirstName" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="FirstName" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" CssClass="text-danger" EnableClientScript="False" ErrorMessage="First Name is required." Display="Dynamic">*</asp:RequiredFieldValidator>
            </div>

        <asp:Label runat="server" Text="Last Name" AssociatedControlID="LastName" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="LastName" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Last Name is required." Display="Dynamic">*</asp:RequiredFieldValidator>
            </div>

        </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Account #" AssociatedControlID="AccountNumber" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="AccountNumber" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
            <asp:CustomValidator ID="cvAccountNumber" runat="server" ControlToValidate="AccountNumber" CssClass="text-danger" EnableClientScript="False" ErrorMessage="The account number does not match the client's last name." OnServerValidate="cvAccountNumber_ServerValidate" ValidateEmptyText="True" Display="Dynamic">*</asp:CustomValidator>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="AccountNumber" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Account number is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

        <asp:Label runat="server" Text="HKID/Passport#" AssociatedControlID="HKID" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="HKID" runat="server" CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="HKID" CssClass="text-danger" EnableClientScript="False" ErrorMessage="HKID/Passport number is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

        <asp:Label runat="server" Text="Passport country of issue" AssociatedControlID="passport_country_of_issue" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="passport_country_of_issue" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Date of Birth" AssociatedControlID="DateOfBirth" CssClass="control-label col-md-2"></asp:Label>
         <div class="col-md-4"><asp:TextBox ID="DateOfBirth" runat="server" CssClass="form-control"></asp:TextBox>
             <asp:RegularExpressionValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Date of Birth is not valid." ForeColor="Red" ValidationExpression="=&quot;^(((0?[1-9]|1[012])/(0?[1-9]|1\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\d)\d{2}|0?2/29/((19|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$" Display="Dynamic">*</asp:RegularExpressionValidator>
             <asp:RequiredFieldValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Date of birth is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

        <asp:Label runat="server" Text="Email" AssociatedControlID="Email" CssClass="control-label col-md-2"></asp:Label>
         <div class="col-md-4"><asp:TextBox ID="Email" runat="server" CssClass="form-control" MaxLength="25" TextMode="Email"></asp:TextBox>
             <asp:RequiredFieldValidator runat="server" ControlToValidate="Email" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Email address is required" Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

    </div>

    <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="Building" Text="Building" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="Building" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>

        <asp:Label runat="server" AssociatedControlID="Street" Text="Street" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="Street" runat="server" CssClass="form-control" MaxLength="35"></asp:TextBox>
        </div>

        <asp:Label runat="server" AssociatedControlID="District" Text="District" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="District" runat="server" CssClass="form-control" MaxLength="19"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="home_phone" Text="Home Phone" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="home_phone" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>
        
        <asp:Label runat="server" AssociatedControlID="home_fax" Text="Home Fax" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="home_fax" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>


    </div>

    <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="business_phone" Text="Business Phone" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="business_phone" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>
        
        <asp:Label runat="server" AssociatedControlID="mobile_phone" Text="Mobile Phone" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="mobile_phone" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="country_of_citizenship" Text="Country of citizenship" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="country_of_citizenship" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
        </div>
        
        <asp:Label runat="server" AssociatedControlID="country_of_residence" Text="Country of legal residence" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="country_of_residence" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
        </div>

    </div>
    
    <h4>Employment Information</h4>

    <div class="form-group">

        <asp:Label runat="server" Text="Employment Status" CssClass="control-label col-md-2"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">Employment Status</asp:ListItem>
                <asp:ListItem Value="employed">Employed</asp:ListItem>
                <asp:ListItem Value="self_employed">Self-employed</asp:ListItem>
                <asp:ListItem Value="retired">Retired</asp:ListItem>
                <asp:ListItem Value="student">Student</asp:ListItem>
                <asp:ListItem Value="not_employed">Not Employed</asp:ListItem>
                <asp:ListItem Value="homemaker">Homemaker</asp:ListItem>
            </asp:DropDownList>

    </div>

    <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="specifc_occupation" Text="Specific occupation" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="specifc_occupation" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
        </div>

        <asp:Label runat="server" AssociatedControlID="years" Text="Years with employer" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="years" runat="server" CssClass="form-control" MaxLength="2"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="employer_name" Text="Employer name" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="employer_name" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
        </div>

        <asp:Label runat="server" AssociatedControlID="employer_phone" Text="Employer phone" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="employer_phone" runat="server" CssClass="form-control" MaxLength="2"></asp:TextBox>
        </div>

    </div>

     <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="nature" Text="Nature of business" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="nature" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
        </div>


    </div>

    <h4>&nbsp;</h4>
    
    <div class="form-group">
        <div class="col-md-4">
            </div>
    </div>
   
    <h4>Investment Profile</h4>

    <div class="form-group">

        <asp:Label runat="server" Text="Investment objective* for this account:" CssClass="control-label col-md-2"></asp:Label>
        <asp:DropDownList ID="ddlobjctivesver" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                <asp:ListItem Value="preservation">capital preservation</asp:ListItem>
                <asp:ListItem Value="income">income</asp:ListItem>
                <asp:ListItem Value="growth">growth</asp:ListItem>
                <asp:ListItem Value="speculation">speculation</asp:ListItem>
            </asp:DropDownList>

    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Investment knowledge:" CssClass="control-label col-md-2"></asp:Label>
        <asp:DropDownList ID="ddlknowledge" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                <asp:ListItem Value="none">none</asp:ListItem>
                <asp:ListItem Value="limited">limited</asp:ListItem>
                <asp:ListItem Value="good">good</asp:ListItem>
                <asp:ListItem Value="extensive">extensive</asp:ListItem>
            </asp:DropDownList>

    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Investment experience:" CssClass="control-label col-md-2"></asp:Label>
        <asp:DropDownList ID="ddlexperience" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                <asp:ListItem Value="none">none</asp:ListItem>
                <asp:ListItem Value="limited">limited</asp:ListItem>
                <asp:ListItem Value="good">good</asp:ListItem>
                <asp:ListItem Value="extensive">extensive</asp:ListItem>
            </asp:DropDownList>

    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Annual income:" CssClass="control-label col-md-2"></asp:Label>
        <asp:DropDownList ID="ddlincome" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                <asp:ListItem Value="20000<">under HK$20,000</asp:ListItem>
                <asp:ListItem Value="20001-200000">HK$20,001 - HK$200,000</asp:ListItem>
                <asp:ListItem Value="200001-2000000">HK$200,001 - HK$2,000,000</asp:ListItem>
                <asp:ListItem Value=">2000000">more than HK$2,000,000</asp:ListItem>
            </asp:DropDownList>

    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Approximate liquid net worth (cash and securities):" CssClass="control-label col-md-2"></asp:Label>
        <asp:DropDownList ID="ddlnetworth" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                <asp:ListItem Value="100000<">under HK$100,000</asp:ListItem>
                <asp:ListItem Value="100001-1000000">HK$100,001 - HK$1,000,000</asp:ListItem>
                <asp:ListItem Value="1000001-10000000">HK$1,000,001 - HK$10,000,000</asp:ListItem>
                <asp:ListItem Value=">10000000">more than HK$10,000,000</asp:ListItem>
            </asp:DropDownList>

    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="User Name" AssociatedControlID="UserName" CssClass="control-label col-md-2"></asp:Label>
         <div class="col-md-4"><asp:TextBox ID="UserName" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
             <asp:RegularExpressionValidator runat="server" ControlToValidate="UserName" CssClass="text-danger" EnableClientScript="False" ErrorMessage="User Name must contain at least 6 characters." ForeColor="Red" ValidationExpression="^[\s\S]{6,8}$" Display="Dynamic">*</asp:RegularExpressionValidator>
             <asp:RegularExpressionValidator runat="server" ControlToValidate="UserName" CssClass="text-danger" EnableClientScript="False" ErrorMessage="User Name must contain only letters and digits" ForeColor="Red" ValidationExpression="^[a-zA-Z0-9]+$" Display="Dynamic">*</asp:RegularExpressionValidator>
             <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName" CssClass="text-danger" EnableClientScript="False" ErrorMessage="User name is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Password" AssociatedControlID="Password" CssClass="control-label col-md-2"></asp:Label>
         <div class="col-md-4"><asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control" MaxLength="15"></asp:TextBox>
             <asp:RegularExpressionValidator runat="server" ControlToValidate="Password" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Password must contain at least 8 characters." ForeColor="Red" ValidationExpression="^[\s\S]{8,15}$" Display="Dynamic">*</asp:RegularExpressionValidator>
             <asp:RegularExpressionValidator runat="server" ControlToValidate="Password" EnableClientScript="False" ErrorMessage="Password must contain at least 2 non-alphanumeric characters." ForeColor="Red" Display="Dynamic" ValidationExpression="^[\W]{2,15}$">*</asp:RegularExpressionValidator>
             <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Password is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

        <asp:Label runat="server" Text="Confirm Password" AssociatedControlID="ConfirmPassword" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" CssClass="form-control" MaxLength="15"></asp:TextBox>
            <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Password and Confirm Password do not match." Display="Dynamic">*</asp:CompareValidator>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Confirm Password is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10"><asp:Button ID="Register" runat="server" Text="Register" CssClass="btn button-default" /></div>
    </div>
    </div>

</asp:Content>
