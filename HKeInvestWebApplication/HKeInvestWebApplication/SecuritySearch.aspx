﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecuritySearch.aspx.cs" Inherits="HKeInvestWebApplication.SecuritySearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h4>Security Search</h4>
    <div class="form-horizontal">
        <!--Search field-->
        <div class="form-group">
            <asp:Label runat="server" Text="Security Type" AssociatedControlID="SecurityType" CssClass="col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:DropDownList CssClass="form-control" ID="SecurityType" runat="server" OnSelectedIndexChanged="SecurityType_changed" AutoPostBack="True">
                    <asp:ListItem Value="0">Security type</asp:ListItem>
                    <asp:ListItem Value="bond">Bond</asp:ListItem>
                    <asp:ListItem Value="stock">Stock</asp:ListItem>
                    <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                </asp:DropDownList>
            </div>

            <asp:Label CssClass="col-md-3" runat="server" Text="Security Code" ID="SecurityCodeLabel" Visible="False" AssociatedControlID="SecurityCode"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox CssClass="form-control" ID="SecurityCode" runat="server" MaxLength="4" AutoPostBack="True" Visible="False" OnTextChanged="SecurityCode_changed"></asp:TextBox>     
            </div>
        </div>
        <div class="form-group">

                <asp:Label CssClass="col-md-3" runat="server" Text="Security Name" ID="SecurityNameLabel" Visible="False" AssociatedControlID="SecurityName"></asp:Label>
            <div class="col-md-9">
                <asp:TextBox CssClass="form-control" ID="SecurityName" runat="server" Visible="False" OnTextChanged="SecurityName_changed" AutoPostBack="true"></asp:TextBox>
            </div>
        </div>
    </div>
        <div>
            <asp:Label ID="MessageLabel" runat="server" Text="No securities match your searching criteria." Visible="false"></asp:Label>
        </div>
        <!--Result field-->
        <div>
            <asp:GridView ID="ResultGridView" runat="server" Visible="False" AutoGenerateColumns="False" OnSorting="ResultGridView_Sorting" AllowSorting="True" CssClass="table table-bordered">
                <Columns>
                    <asp:BoundField Visible="false" HeaderText="Code" DataField="code" SortExpression="code"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="name" HeaderText="Name" SortExpression="name"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="price" DataFormatString="{0:n2}" HeaderText="Price"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="changeDollar" HeaderText="Change Dollar"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="high" HeaderText="High" DataFormatString="{0:n2}" SortExpression="high"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="low" HeaderText="Low" DataFormatString="{0:n2}" SortExpression="low"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="oneYear" DataFormatString="{0:n2}" HeaderText="One Year"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="riskReturn" HeaderText="Risk Return"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="launchDate" HeaderText="Launch Date"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="yield" HeaderText="Yield"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="changePercent" HeaderText="Change Percent"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="volume" HeaderText="Volume"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="base" HeaderText="Base"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="sinceLaunch" HeaderText="Since Launch"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="peRatio" HeaderText="PE Ratio"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="threeYears" HeaderText="Three Years"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="close" HeaderText="Close"></asp:BoundField>
                    <asp:BoundField Visible="false" DataField="sixMonths" HeaderText="Six Months"></asp:BoundField>
                    <asp:BoundField Visible="false"  DataField="size" HeaderText="Size"></asp:BoundField>
                </Columns>
            </asp:GridView>
    </div>
</asp:Content>
