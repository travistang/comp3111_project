﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HKeInvestWebApplication
{
    public partial class RegistrationPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cvAccountNumber_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string account_number = AccountNumber.Text.Trim();
            string last_name = LastName.Text.Trim();
            string lastname = last_name.ToString().ToUpper();
            char first_letter;
            char second_letter;

            if (lastname != "")
            {
                first_letter = lastname[0];
                second_letter = lastname[1];

                if (account_number.Contains(first_letter) == false || (account_number.Contains(first_letter) && account_number.Contains(second_letter)) == false)
                {
                    args.IsValid = false;
                    cvAccountNumber.ErrorMessage = "The account number does not match the client's last name.";
                }
                else
                {
                    foreach (char c in account_number.Substring(2, 9))
                    {
                        if (c < '0' || c > '9')
                        {
                            args.IsValid = false;
                            cvAccountNumber.ErrorMessage = "The account number does not match the client's last name.";

                        }

                    }
                }
            }

            

        }
    }
}