﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using HKeInvestWebApplication.Models;
using System.Data;
using System.Data.SqlClient;
using HKeInvestWebApplication.Code_File;



namespace HKeInvestWebApplication.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() { UserName = UserName.Text, Email = Email.Text };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //string code = manager.GenerateEmailConfirmationToken(user.Id);
                //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                string firstname = FirstName.Text.Trim();
                string lastname = LastName.Text.Trim();
                string hkid = HKID.Text.Trim();
                string birth = DateOfBirth.Text.Trim();
               
                string sql = "select count(*) where (Client.FirstName = '" + firstname + "', Client.LastName = '" + lastname + "', Client.HKID = '" + hkid + "', dateOfBirth=CONVERT(date, '" + DateOfBirth.Text + "', 103) and Client.DateOfBirth = '" + birth + "')";

                //string sql2 = "insert into [Clients] ([firstName],[lastName],[HKIDPassportNumber],[dateOfBirth]) values ('" + firstname + "','" + lastname + "','" + hkid + "','" + birth + "')";

                HKeInvestData myHKeInvestData = new HKeInvestData();

                IdentityResult roleResult = manager.AddToRole(user.Id, "Client");
                if(!roleResult.Succeeded)
                {
                    ErrorMessage.Text = roleResult.Errors.FirstOrDefault();
                }
                AddClientRecord(UserName.Text.Trim());
                if (myHKeInvestData.getAggregateValue(sql) != -1)
                {
                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
            }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
        private void AddClientRecord(string username)
        {
            string firstname = FirstName.Text.Trim();
            string lastname = LastName.Text.Trim();
            string hkid = HKID.Text.Trim();
            string birth = DateOfBirth.Text.Trim();
            string email = Email.Text.Trim();

            int x = 0;
            string newno =  x.ToString();


             string sql3 = lastname.Substring(0, 2);
               string sql4 = (sql3+newno).Trim();

            string accountNumber = newno.Trim(); 

            string accountType = ddlAccountType.Text;
            string balance = "20000.00";
            HKeInvestData myHKeInvestData = new HKeInvestData();
            SqlTransaction trans = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData("insert into [Account] ([userName] , [accountNumber] , [accountType] , [balance]) values ('" + username + "','" + accountNumber + "','" + accountType + "','" + balance + "')", trans);
            myHKeInvestData.setData("insert into [Client] ([firstName] , [lastName] , [HKIDPassportNumber] , [dateOfBirth] , [email] , [accountNumber]) values ('" + firstname + "','" + lastname + "','" + hkid + "','" + birth + "','" + email + "','" + accountNumber + "')", trans);
            myHKeInvestData.commitTransaction(trans);
        }

  

        protected void cvAccountNumber_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            string account_number = AccountNumber.Text.Trim();
            string last_name = LastName.Text.Trim();
            string lastname = last_name.ToString().ToUpper();
            char first_letter;
            char second_letter;

            if (lastname != "")
            {
                first_letter = lastname[0];
                second_letter = lastname[1];

                if (account_number.Contains(first_letter) == false || (account_number.Contains(first_letter) && account_number.Contains(second_letter)) == false)
                {
                    args.IsValid = false;
                    cvAccountNumber.ErrorMessage = "The account number does not match the client's last name.";
                }
               
            }
        }
    }
}