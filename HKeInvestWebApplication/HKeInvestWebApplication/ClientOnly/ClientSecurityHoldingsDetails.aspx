﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientSecurityHoldingsDetails.aspx.cs" Inherits="HKeInvestWebApplication.ClientOnly.ClientSecurityHoldingsDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h4>Your Security Holding Details</h4>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-md-4">
                <asp:Label ID="txtAccountNumber" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:DropDownList ID="ddlSecurityType" runat="server" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="ddlSecurityType_SelectedIndexChanged">
                    <asp:ListItem Value="0">Security type</asp:ListItem>
                    <asp:ListItem Value="bond">Bond</asp:ListItem>
                    <asp:ListItem Value="stock">Stock</asp:ListItem>
                    <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-4">
                <asp:DropDownList ID="ddlCurrency" runat="server" AutoPostBack="True" Visible="False" CssClass="form-control" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                    <asp:ListItem Value="0">Currency</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div>
            <asp:Label ID="lblClientName" runat="server" Text="" Visible="False" CssClass="col-md-6"></asp:Label>
            <asp:Label ID="lblResultMessage" runat="server" Text="" Visible="False" CssClass="col-md-6"></asp:Label>
        </div>
        <div>
            <asp:GridView ID="gvSecurityHolding" runat="server" Visible="False" AutoGenerateColumns="False" OnSorting="gvSecurityHolding_Sorting" AllowSorting="True" CssClass="table table-bordered">
                <Columns>
                    <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" SortExpression="code"></asp:BoundField>
                    <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="name"></asp:BoundField>
                    <asp:BoundField DataField="shares" DataFormatString="{0:n2}" HeaderText="Shares" ReadOnly="True" SortExpression="shares"></asp:BoundField>
                    <asp:BoundField DataField="base" HeaderText="Base" ReadOnly="True"></asp:BoundField>
                    <asp:BoundField DataField="price" DataFormatString="{0:n2}" HeaderText="Price" ReadOnly="True"></asp:BoundField>
                    <asp:BoundField DataField="value" DataFormatString="{0:n2}" HeaderText="Value" ReadOnly="True" SortExpression="value"></asp:BoundField>
                    <asp:BoundField DataField="convertedValue" DataFormatString="{0:n2}" HeaderText="Value in" ReadOnly="True" SortExpression="convertedValue"></asp:BoundField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

